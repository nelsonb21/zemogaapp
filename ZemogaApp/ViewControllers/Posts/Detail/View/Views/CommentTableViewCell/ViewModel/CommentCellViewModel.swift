//
//  CommentCellViewModel.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation

class CommentCellViewModel {

    // MARK: - Properties

    let name: Bindable<String?> = Bindable(nil)
    let body: Bindable<String?> = Bindable(nil)

    // MARK: - Life Cycle

    init(comment: Comment) {
        name.value = comment.name
        body.value = comment.body
    }
    
}

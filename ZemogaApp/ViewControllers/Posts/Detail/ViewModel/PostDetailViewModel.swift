//
//  PostDetailViewModel.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit

class PostDetailViewModel {

    // MARK: - Constants

    private struct Constants {
        static let favoriteIconFill = #imageLiteral(resourceName: "icon_favoriteFill")
        static let favoriteIconEmpty = #imageLiteral(resourceName: "icon_favoriteEmpty")
    }

    // Properties

    let post: Post
    let favoriteButtonImage: Bindable<UIImage> = Bindable(Constants.favoriteIconEmpty)
    let title: Bindable<String?> = Bindable("")
    let body: Bindable<String?> = Bindable("")
    let username: Bindable<String?> = Bindable("")
    let address: Bindable<String?> = Bindable("")
    let comments: Bindable<[Comment]> = Bindable([])
    private let requestManager: RequestManagerProtocol
    private let postStore: PostStoreProtocol

    // MARK: - Life Cycle

    init(post: Post, requestManager: RequestManagerProtocol, postStore: PostStoreProtocol) {
        self.post = post
        self.requestManager = requestManager
        self.postStore = postStore
        title.value = post.title
        body.value = post.body
        setupFavoriteButton()
        retriveUser()
        retriveComments()
    }

    // MARK: - Public Methods

    func favoritePost() {
        post.isFavorite = !post.isFavorite
        postStore.save(post: post)
        setupFavoriteButton()
    }

    // MARK: - Private Methods

    private func setupFavoriteButton() {
        favoriteButtonImage.value = post.isFavorite ? Constants.favoriteIconFill : Constants.favoriteIconEmpty
    }

    private func retriveUser() {
        requestManager.get(path: .user(id: "\(post.userId)")) { [weak self] data, error in
            guard error == nil, let data = data as? [String: Any], let `self` = self else {
                return
            }

            let user = User(data: data)
            self.setupUserInformation(with: user)
        }
    }

    private func setupUserInformation(with user: User) {
        username.value = user.username
        address.value = "\(user.address.street ?? ""), \(user.address.city ?? "")"
    }

    private func retriveComments() {
        requestManager.get(path: .comments(postId: "\(post.id)")) { [weak self] data, error in
            guard error == nil, let data = data as? [[String: Any]], let `self` = self else {
                return
            }

            var comments: [Comment] = []

            data.forEach({ commentData in
                comments.append(Comment(data: commentData))
            })

            self.comments.value = comments
        }
    }

}

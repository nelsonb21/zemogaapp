//
//  PostStore.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation
import RealmSwift

protocol PostStoreProtocol {
    func save(post: Post)
    func save(posts: [Post])
    func delete(post: Post)
    func delete(posts: [Post])
    func deleteAll()
    func list() -> [Post]
}

class PostStore: NSObject, PostStoreProtocol {

    // MARK: - Properties
    
    static let shared = PostStore()
    private let realm = try! Realm()

    // MARK: - Life Cycle

    private override init() {
        super.init()
    }

    // MARK: - Public Methods

    func save(post: Post) {
        try? realm.write {
            let storeModels = RealmStore()
            storeModels.id = "\(post.id)"
            storeModels.model = "post"
            storeModels.object = post.toJSON()
            realm.add(storeModels, update: true)
        }
    }

    func save(posts: [Post]) {
        posts.forEach { post in
            save(post: post)
        }
    }

    func delete(post: Post) {
        guard let storeModel = realm.objects(RealmStore.self).filter("model == 'post' AND id == '\(post.id)'").first else { return }
        try? realm.write {
            realm.delete(storeModel)
        }
    }

    func delete(posts: [Post]) {
        posts.forEach { post in
            delete(post: post)
        }
    }

    func deleteAll() {
        try? realm.write {
            let storeModels = realm.objects(RealmStore.self).filter("model == 'post'")
            realm.delete(storeModels)
        }
    }

    func list() -> [Post] {
        let storeModels = realm.objects(RealmStore.self).filter("model == 'post'")
        var posts = [Post]()
        storeModels.forEach { storeModel in
            if let object = storeModel.object {
                posts.append(Post(data: object))
            }
        }

        posts.sort { (post1, post2) -> Bool in
            return post1.id < post2.id
        }

        return posts
    }

    private func getPost(with id: String) -> Post? {
        guard let storeModel = realm.objects(RealmStore.self).filter("model == 'post' AND id == '\(id)'").first, let object = storeModel.object else {
            return nil
        }

        return Post(data: object)
    }

}

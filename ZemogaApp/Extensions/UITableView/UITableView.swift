//
//  UITableView.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit

extension UITableView {

    func register(withIdentifier identifier: String) {
        self.register(withIdentifier: identifier, nibIdentifier: identifier)
    }

    func register(withIdentifiers identifiers: [String]) {
        for identifier in identifiers {
            self.register(withIdentifier: identifier, nibIdentifier: identifier)
        }
    }

    func register(withIdentifier identifier: String, nibIdentifier: String) {
        self.register(UINib(nibName: nibIdentifier, bundle: nil), forCellReuseIdentifier: identifier)
    }

}

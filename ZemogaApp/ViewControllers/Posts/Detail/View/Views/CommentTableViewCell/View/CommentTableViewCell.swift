//
//  CommentTableViewCell.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    // MARK: - IBOutlets

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!

    // MARK: - Properties

    var viewModel: CommentCellViewModel! {
        didSet {
            viewModel.name.bindAndFire { [weak self] name in
                self?.nameLabel.text = name
            }

            viewModel.body.bindAndFire { [weak self] body in
                self?.bodyLabel.text = body
            }
        }
    }

    // MARK: - Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
    }

}

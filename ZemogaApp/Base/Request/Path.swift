//
//  Path.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation

enum Path: Router {
    case posts
    case user(id: String)
    case comments(postId: String)

    var config: APIConfig {
        return .default
    }

    var query: APIQuery {
        switch(self) {
        case .posts:
            return APIQuery(httpMethod: .get, path: "posts")
        case .user(let id):
            return APIQuery(httpMethod: .get, path: "users/\(id)")
        case .comments(let postId):
            return APIQuery(httpMethod: .get, path: "comments", parameters: ["postId": postId])
        }
    }

}

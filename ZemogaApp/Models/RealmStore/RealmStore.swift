//
//  RealmStore.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class RealmStore: Object {

    @objc dynamic var data: Data?
    @objc dynamic var model: String?
    @objc dynamic var id: String?

    var object: [String : Any]? {
        get {
            guard let data = data else { return nil }
            do {
                return try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [String : Any]
            } catch {
                fatalError()
            }
        }
        set {
            guard let value = newValue else { return }
            do {
                data = try NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: true)
            } catch {
                fatalError()
            }
        }
    }

    //MARK: - Init

    required init() {
        super.init()
    }

    //MARK: - Realm

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }

    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }

    override class func primaryKey() -> String? {
        return "id"
    }
}

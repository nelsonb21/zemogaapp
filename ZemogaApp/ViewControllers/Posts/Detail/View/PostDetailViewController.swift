//
//  PostDetailViewController.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit

class PostDetailViewController: UIViewController {

    // MARK: - IBOutlets

    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(withIdentifier: CommentTableViewCell.reuseIdentifier)
            tableView.dataSource = self
        }
    }

    // MARK: - Properties
    
    override class var storyboardID: String? {
        return String(describing: self)
    }

    override class var storyboardName: String {
        return "PostDetail"
    }

    var viewModel: PostDetailViewModel! {
        didSet {
            loadViewIfNeeded()
            
            viewModel.username.bindAndFire { [weak self] username in
                self?.usernameLabel.text = username
            }

            viewModel.address.bindAndFire { [weak self] address in
                self?.addressLabel.text = address
            }

            viewModel.title.bindAndFire { [weak self] title in
                self?.titleLabel.text = title
            }

            viewModel.body.bindAndFire { [weak self] body in
                self?.bodyLabel.text = body
            }

            viewModel.favoriteButtonImage.bindAndFire { [weak self] image in
                self?.favoriteButton.setImage(image, for: .normal)
            }

            viewModel.comments.bindAndFire { [weak self] _ in
                self?.tableView.reloadData()
            }
        }
    }

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - IBActions

    @IBAction func didTapFavoriteButton(_ sender: UIButton) {
        viewModel.favoritePost()
    }

}

extension PostDetailViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.comments.value.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CommentTableViewCell.reuseIdentifier, for: indexPath) as! CommentTableViewCell
        cell.viewModel = CommentCellViewModel(comment: viewModel.comments.value[indexPath.row])
        return cell
    }
}

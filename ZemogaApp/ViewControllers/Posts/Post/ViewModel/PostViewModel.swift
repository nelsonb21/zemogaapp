//
//  PostViewModel.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation

class PostViewModel {

    // MARK: - Constants

    private struct Constants {
        static let unreadPostsMaxAmount = 20
    }

    // MARK: - Properties

    let posts: Bindable<[Post]> = Bindable([])
    private let requestManager: RequestManagerProtocol
    private let postStore: PostStoreProtocol
    private var shouldFilterFavoritePosts = false

    // MARK: - Events

    var resetView: (() -> ())?

    // MARK: - Life Cycle

    init(requestManager: RequestManagerProtocol, postStore: PostStoreProtocol) {
        self.requestManager = requestManager
        self.postStore = postStore
    }

    // MARK: - Public Methods

    func viewDidApper() {
        getPosts()
    }

    func reloadPosts() {
        deletePosts()
        getPosts()
        resetView?()
    }

    func deletePosts() {
        postStore.deleteAll()
        posts.value = []
    }

    func updatePost(at index: Int) {
        let post = posts.value[index]
        post.isUnread = false
        postStore.save(post: post)
        getPosts()
    }

    func deletePost(at index: Int) {
        let post = posts.value[index]
        postStore.delete(post: post)
    }

    func makePostDetailViewController(with index: Int) -> PostDetailViewController {
        let viewController = PostDetailViewController.instantiateFromStoryboard()
        let post = posts.value[index]
        let viewModel = PostDetailViewModel(post: post, requestManager: requestManager, postStore: postStore)
        viewController.viewModel = viewModel
        return viewController
    }

    func filterFavoritePosts() {
        shouldFilterFavoritePosts = !shouldFilterFavoritePosts

        if shouldFilterFavoritePosts {
            posts.value = posts.value.filter { $0.isFavorite }
        } else {
            getPosts()
        }
    }

    // MARK: - Private Methods

    private func getPosts() {
        posts.value = postStore.list()

        guard posts.value.isEmpty else {
            return
        }
        
        requestManager.get(path: .posts) { [weak self] data, error in
            guard error == nil, let data = data as? [[String: Any]], let `self` = self else {
                return
            }

            var posts: [Post] = []

            data.forEach({ postData in
                posts.append(Post(data: postData))
            })

            posts = self.markUnreadPosts(posts)
            self.postStore.save(posts: posts)
            self.posts.value = posts
        }
    }

    private func markUnreadPosts(_ posts: [Post]) -> [Post] {
        var index = 0
        posts.forEach({
            $0.isUnread = index < Constants.unreadPostsMaxAmount
            index = index + 1
        })

        return posts
    }

}

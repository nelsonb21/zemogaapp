//
//  PostViewController.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit

class PostViewController: UIViewController {

    // MARK: - IBOutlets

    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.register(withIdentifier: PostTableViewCell.reuseIdentifier)
            tableView.dataSource = self
            tableView.delegate = self
        }
    }

    @IBOutlet weak var segmentedControl: UISegmentedControl!

    // MARK: - Properties

    var viewModel: PostViewModel! {
        didSet {
            viewModel.posts.bindAndFire { [weak self] newPosts in
                self?.posts = newPosts
                self?.tableView.reloadData()
            }

            viewModel.resetView = { [weak self] in
                self?.segmentedControl.selectedSegmentIndex = 0
            }
        }
    }

    private var posts: [Post] = []

    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = PostViewModel(requestManager: RequestManager.shared, postStore: PostStore.shared)
    }

    override func viewDidAppear(_ animated: Bool) {
        viewModel.viewDidApper()
    }

    // MARK: - IBActions

    @IBAction private func didValueChangeOnFilterSegmentedControl(_ sender: UISegmentedControl) {
        viewModel.filterFavoritePosts()
    }

    @IBAction private func didTapDeleteAllButton(_ sender: UIButton) {
        viewModel.deletePosts()
    }

    @IBAction private func didTapReloadBarButtonItem(_ sender: UIBarButtonItem) {
        viewModel.reloadPosts()
    }

}

extension PostViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostTableViewCell.reuseIdentifier, for: indexPath) as! PostTableViewCell
        cell.viewModel = PostCellViewModel(post: posts[indexPath.row])
        return cell
    }

}

extension PostViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.updatePost(at: indexPath.row)
        let viewController = viewModel.makePostDetailViewController(with: indexPath.row)
        navigationController?.pushViewController(viewController, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deletePostAction = UITableViewRowAction(style: .destructive, title: "Delete") { _, indexPath in
            self.viewModel.deletePost(at: indexPath.row)
            self.posts.remove(at: indexPath.row)
            let cell = tableView.cellForRow(at: indexPath)
            cell?.backgroundColor = .red

            UIView.animate(withDuration: 2, delay: 0.1 * Double(indexPath.row), animations: {
                cell?.alpha = 1
            }, completion: { _ in
                tableView.deleteRows(at: [indexPath], with: .fade)
            })
        }

        return [deletePostAction]
    }

}

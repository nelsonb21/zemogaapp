//
//  Comment.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation

class Comment {
    var postId: Int
    var id: Int
    var name: String?
    var body: String?

    init(postId: Int, id: Int, name: String?, body: String?) {
        self.postId = postId
        self.id = id
        self.name = name
        self.body = body
    }

    convenience init(data: [String: Any]) {
        let postId = data["postId"] as? Int ?? 0
        let id = data["id"] as? Int ?? 0
        let name = data["name"] as? String
        let body = data["body"] as? String

        self.init(postId: postId, id: id, name: name, body: body)
    }
    
}

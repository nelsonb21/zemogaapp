//
//  PostStoreMock.swift
//  ZemogaAppTests
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation
@testable import ZemogaApp

class PostStoreMock: PostStoreProtocol {

    private var posts: [Post] = []

    func save(post: Post) {
        posts.append(post)
    }

    func save(posts: [Post]) {
        posts.forEach { post in
            save(post: post)
        }
    }

    func delete(post: Post) {
        let firstIndex = posts.firstIndex { $0.id == post.id }

        guard let index = firstIndex else {
            return
        }

        posts.remove(at: index)
    }

    func delete(posts: [Post]) {
        posts.forEach { delete(post: $0) }
    }

    func deleteAll() {
        posts.removeAll()
    }

    func list() -> [Post] {
        return posts
    }

}

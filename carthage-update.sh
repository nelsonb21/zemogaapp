#!/bin/sh

if [ "$1" = "" ] ; then
   carthage update --platform iOS
else
	carthage update --platform iOS $1
fi

cp -r Carthage/Build/iOS/*.framework Frameworks
rm -rf Carthage/Build
rm Cartfile.resolved

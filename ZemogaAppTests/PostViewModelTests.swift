//
//  PostViewModelTests.swift
//  ZemogaAppTests
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import XCTest
@testable import ZemogaApp

class PostViewModelTests: XCTestCase {

    func testViewDidAppearWithValidData() {
        let requestManagerMock = RequestManagerMock(shouldReturnBadData: false)
        let postStoreMock = PostStoreMock()
        let viewModel = PostViewModel(requestManager: requestManagerMock, postStore: postStoreMock)
        viewModel.viewDidApper()

        XCTAssertFalse(viewModel.posts.value.isEmpty, "Posts variable shouldn't be empty")
    }

    func testViewDidAppearWithInvalidData() {
        let requestManagerMock = RequestManagerMock(shouldReturnBadData: true)
        let postStoreMock = PostStoreMock()
        let viewModel = PostViewModel(requestManager: requestManagerMock, postStore: postStoreMock)
        viewModel.viewDidApper()

        XCTAssertTrue(viewModel.posts.value.isEmpty, "Posts variable should be empty")
    }

    func testReloadPosts() {
        let requestManagerMock = RequestManagerMock(shouldReturnBadData: false)
        let postStoreMock = PostStoreMock()
        let viewModel = PostViewModel(requestManager: requestManagerMock, postStore: postStoreMock)
        let resetViewExpectation = expectation(description: "resetView in PostViewModel should be called")
        
        viewModel.resetView = {
            resetViewExpectation.fulfill()
        }

        viewModel.reloadPosts()

        waitForExpectations(timeout: 0.1) { error in
            guard let error = error else {
                return
            }

            XCTFail("resetView in PostViewModel should have been called. \(error)")
        }
    }

    func testDeleteAllPosts() {
        let requestManagerMock = RequestManagerMock(shouldReturnBadData: false)
        let postStoreMock = PostStoreMock()
        let viewModel = PostViewModel(requestManager: requestManagerMock, postStore: postStoreMock)
        viewModel.viewDidApper()

        XCTAssertFalse(viewModel.posts.value.isEmpty, "Posts variable shouldn't be empty")

        viewModel.deletePosts()

        XCTAssertTrue(viewModel.posts.value.isEmpty, "Posts variable should be empty")
    }

    func testUpdatePostUnreadAttribute() {
        let requestManagerMock = RequestManagerMock(shouldReturnBadData: false)
        let postStoreMock = PostStoreMock()
        let viewModel = PostViewModel(requestManager: requestManagerMock, postStore: postStoreMock)
        viewModel.viewDidApper()

        XCTAssertTrue(viewModel.posts.value[0].isUnread, "Posts unread attribute should be true")

        viewModel.updatePost(at: 0)

        XCTAssertFalse(viewModel.posts.value[0].isUnread, "Posts unread attribute should be false")
    }

    func testDeletePost() {
        let requestManagerMock = RequestManagerMock(shouldReturnBadData: false)
        let postStoreMock = PostStoreMock()
        let viewModel = PostViewModel(requestManager: requestManagerMock, postStore: postStoreMock)
        viewModel.viewDidApper()

        XCTAssertTrue(postStoreMock.list().count == 2, "Posts count is equal or higher than 2")

        viewModel.deletePost(at: 1)

        XCTAssertTrue(postStoreMock.list().count == 1, "Posts count is equal or less than 1")
    }

    func testMakePostDetailViewController() {
        let requestManagerMock = RequestManagerMock(shouldReturnBadData: false)
        let postStoreMock = PostStoreMock()
        let viewModel = PostViewModel(requestManager: requestManagerMock, postStore: postStoreMock)
        viewModel.viewDidApper()

        let viewController = viewModel.makePostDetailViewController(with: 0)
        XCTAssertTrue(viewController.viewModel.post.id == viewModel.posts.value[0].id, "Post id in PostDetailViewModel and post in index 0 should match")
    }

    func testFilterFavoritePosts() {
        let requestManagerMock = RequestManagerMock(shouldReturnBadData: false)
        let postStoreMock = PostStoreMock()
        let viewModel = PostViewModel(requestManager: requestManagerMock, postStore: postStoreMock)
        viewModel.viewDidApper()
        viewModel.posts.value[0].isFavorite = true

        viewModel.filterFavoritePosts()

        XCTAssertTrue(viewModel.posts.value.count == 1, "Posts count should equal to 1")

        viewModel.filterFavoritePosts()

        XCTAssertTrue(viewModel.posts.value.count == 2, "Posts count should equal to 2")
    }

}

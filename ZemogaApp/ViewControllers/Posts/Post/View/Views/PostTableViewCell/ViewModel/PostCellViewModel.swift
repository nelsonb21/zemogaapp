//
//  PostCellViewModel.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit

class PostCellViewModel {

    // MARK: - Constants

    private struct Constants {
        static let favoriteIcon = #imageLiteral(resourceName: "icon_favoriteFill")
        static let blueDotIcon = #imageLiteral(resourceName: "icon_blueDot")
    }

    // MARK: - Properties

    let title: Bindable<String?>
    let body: Bindable<String?>
    let icon: Bindable<UIImage?> = Bindable(nil)
    let shouldHideIcon: Bindable<Bool> = Bindable(false)

    // MARK: - Life Cycle

    init(post: Post) {
        title = Bindable(post.title)
        body = Bindable(post.body)
        icon.value = post.isFavorite ? Constants.favoriteIcon : Constants.blueDotIcon
        
        if post.isUnread {
            shouldHideIcon.value = false
        } else {
            shouldHideIcon.value = !post.isFavorite
        }

    }

}

//
//  Post.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation

class Post {
    var userId: Int
    var id: Int
    var title: String?
    var body: String?
    var isFavorite: Bool
    var isUnread: Bool

    init(userId: Int, id: Int, title: String?, body: String?, isFavorite: Bool, isUnread: Bool) {
        self.userId = userId
        self.id = id
        self.title = title
        self.body = body
        self.isFavorite = isFavorite
        self.isUnread = isUnread
    }

    convenience init(data: [String: Any]) {
        let userId = data["userId"] as? Int ?? 0
        let id = data["id"] as? Int ?? 0
        let title = data["title"] as? String ?? ""
        let body = data["body"] as? String ?? ""
        let isFavorite = data["isFavorite"] as? Bool ?? false
        let isUnread = data["isUnread"] as? Bool ?? false

        self.init(userId: userId, id: id, title: title, body: body, isFavorite: isFavorite, isUnread: isUnread)
    }

    func toJSON() -> [String: Any] {
        var json: [String: Any] = [:]
        json["userId"] = userId
        json["id"] = id
        json["title"] = title
        json["body"] = body
        json["isFavorite"] = isFavorite
        json["isUnread"] = isUnread
        return json
    }

}

//
//  APIConfig.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit
import Alamofire

enum APIConfig {
    case `default`
    
    var source: (host: String, headers: [String: String]?) {
        switch self {
        case .default:
            return (host: "https://jsonplaceholder.typicode.com/", headers: nil)
        }
    }
}

//
//  RequestManager.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation
import Alamofire

typealias AppCompletion = (_ object: Any?, _ error: Error?) -> Void

protocol RequestManagerProtocol {
    func get(path: Path, completion: @escaping AppCompletion)
}

class RequestManager: NSObject, RequestManagerProtocol {

    static let shared = RequestManager()

    // MARK: - Life Cycle

    private override init() {
        super.init()
    }

    // MARK: - Public Methods
    
    func get(path: Path, completion: @escaping AppCompletion) {
        AF.request(path).responseJSON { response in
            completion(response.result.value, response.error)
        }
    }

}

//
//  UIViewController+Storyboard.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit

protocol StoryboardInstantiable: class {
    static var storyboardName: String { get }
    static var storyboardID: String? { get }
}

extension StoryboardInstantiable where Self: UIViewController {

    static func instantiateFromStoryboard() -> Self {
        let storyboardViewController: UIViewController?

        if let storyboardID = storyboardID {
            storyboardViewController = UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: storyboardID)
        } else {
            storyboardViewController = UIStoryboard(name: storyboardName, bundle: nil).instantiateInitialViewController()
        }

        guard let viewController = storyboardViewController as? Self else {
            fatalError("Could not instantiate from storyboard for class name: '\(String(describing: self))'")
        }

        return viewController
    }

}

extension UIViewController: StoryboardInstantiable {

    @objc class var storyboardName: String {
        return String(describing: self).deleteSuffix("ViewController")
    }

    @objc class var storyboardID: String? {
        return nil
    }

}

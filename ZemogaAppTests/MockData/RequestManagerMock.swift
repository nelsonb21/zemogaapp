//
//  RequestManagerMock.swift
//  ZemogaAppTests
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation
@testable import ZemogaApp

class RequestManagerMock: RequestManagerProtocol {

    private let mockData: [[String : Any]] = [["userId": 1, "id": 1, "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit", "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"], ["userId": 2, "id": 2, "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit", "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"]]

    private var shouldReturnBadData = false

    init(shouldReturnBadData: Bool) {
        self.shouldReturnBadData = shouldReturnBadData
    }

    func get(path: Path, completion: @escaping AppCompletion) {
        guard !shouldReturnBadData else {
            completion(nil, nil)
            return
        }

        completion(mockData, nil)
    }

}

//
//  Router.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation
import Alamofire

protocol Router: URLRequestConvertible {
    var config: APIConfig { get }
    var query: APIQuery { get }
}

extension Router {
    
    //MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try config.source.host.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(query.path))
        urlRequest.httpMethod = query.httpMethod.rawValue
        urlRequest.addHTTPHeaders(config.source.headers)
        urlRequest.addHTTPHeaders(query.headers)
        let encoding = query.paremeterEncoding
        print ("\(query.httpMethod.rawValue) \(try encoding.encode(urlRequest, with: query.parameters))")
        return try encoding.encode(urlRequest, with: query.parameters)
    }
    
}

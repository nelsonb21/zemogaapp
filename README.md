ZemogaApp
=======

Unit tests are available for `PostViewModel`.


Architecture
=======

The Project was developed following MVVM principles. 

Additions to plain MVVM
=======

**API Directory**

Configuration of the RequestManager.

**Bindable**

The Bindable class provide the communication between the viewModel and the ViewController.

**RealmStore**

The RealStore class provide a generic Model to store all type of data needed.

**PostStore**

The PostStore class provide a set of method to handle persitance to the Post model.


Third Party Libraries
=======

Alamofire - Used to handle requests to the API.
Realm - Used to handle persistance.


Dependencies Manager
=======

Carthage


Running The Project
=======

Download repo and should run without problems.

Requirements
=======

Xcode 10.1
Swift 4.2
Deplyment Target 12.0

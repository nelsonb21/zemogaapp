//
//  PostTableViewCell.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    // MARK: - IBOutlets

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var bodyLabel: UILabel!
    @IBOutlet private weak var iconImageView: UIImageView!

    // MARK: - Properties

    var viewModel: PostCellViewModel! {
        didSet {
            viewModel.title.bindAndFire { [weak self] title in
                self?.titleLabel.text = title
            }

            viewModel.body.bindAndFire { [weak self] body in
                self?.bodyLabel.text = body
            }

            viewModel.icon.bindAndFire { [weak self] icon in
                self?.iconImageView.image = icon
            }

            viewModel.shouldHideIcon.bindAndFire { [weak self] shouldHideIcon in
                self?.iconImageView.isHidden = shouldHideIcon
            }
        }
    }

    // MARK: Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
    }

}

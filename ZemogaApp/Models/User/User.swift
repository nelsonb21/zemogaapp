//
//  USer.swift
//  ZemogaApp
//
//  Created by Nelson Enrique Bolivar Rodriguez on 2/9/19.
//  Copyright © 2019 Nelson Enrique Bolivar Rodriguez. All rights reserved.
//

import Foundation

class User {
    var id: Int
    var username: String?
    var address: Address

    init(id: Int, username: String?, address: Address) {
        self.id = id
        self.username = username
        self.address = address
    }

    convenience init(data: [String: Any]) {
        let id = data["id"] as? Int ?? 0
        let username = data["username"] as? String? ?? ""
        let address = Address(data: data["address"] as! [String: Any])

        self.init(id: id, username: username, address: address)
    }
}

class Address {
    var street: String?
    var city: String?

    init(street: String?, city: String?) {
        self.street = street
        self.city = city
    }

    convenience init(data: [String: Any]) {
        let street = data["street"] as? String
        let city = data["city"] as? String

        self.init(street: street, city: city)
    }
}
